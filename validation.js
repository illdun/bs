
      function validate()
      {
		   var email = document.contact_form.email.value;
            var regex = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;

      
         if( document.contact_form.name.value == "" )
         {
            alert( "Please provide your name!" );
            document.contact_form.name.focus() ;
            return false;
         }
         
         if(!regex.test(email)){
                alert("Enter a valid email");
                return false;
                }
         
            
         if( document.contact_form.message.value == "" )
         {
            alert( "Please provide your message!" );
            return false;
         }
         return( true );
      }
  